﻿using System;
using Xamarin.Forms;

namespace TheGrades
{
	public enum ViewOption
	{
		None,
		View,
		AddNew,
		Edit,
	};

	public interface IViewModel
	{
		INavigation Navigation { get; set; }
	}
}

