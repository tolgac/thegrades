// Created by tcevik
// Property of keskinzeka.com

using System;

namespace TheGrades
{
	public class ClassModel : TheModel.Class
	{
		// member properties
		public string GradeBranch { get { return base.Grade + "-" + base.Branch; } }
        public string ShortClassName { get { return Grade + Branch + " " + ClassName.Substring(0, 3); } }
        public string FullName { get { return Grade + Branch + " " + ClassName;} }

        public TheModel.Class GetClass()
        {
            TheModel.Class aClass = new TheModel.Class();
            aClass.ID = base.ID;
            aClass.ClassName = base.ClassName;
            aClass.Grade = base.Grade;
            aClass.Branch = base.Branch;
            aClass.SchoolID = base.SchoolID;
            aClass.SchoolName = base.SchoolName;
            return aClass;
        }

        // member functions
        public ClassModel( TheModel.Class aClass )
		{
			base.ID = aClass.ID;
			base.ClassName = aClass.ClassName;
			base.Grade = aClass.Grade;
			base.Branch = aClass.Branch;
			base.SchoolID = aClass.SchoolID;
			base.SchoolName = aClass.SchoolName;
		}


	}
}
