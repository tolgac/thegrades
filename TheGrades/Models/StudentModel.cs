using System;
using TheModel;
namespace TheGrader
{
    public class StudentModel : TheModel.Student
    {
        // member properties
        //public string StudentNumberStr
        //{
        //	get { return base.StudenNumber.ToString(); } 
        //	set { base.StudenNumber = int.Parse( value ); }
        //}

        #region Properties
        public string FullName { get { return base.Name + " " + base.LastName; } }
        #endregion

        #region constructors
        public StudentModel(TheModel.Student aStudent)
        {
            base.ID = aStudent.ID;
            base.Name = aStudent.Name;
            base.LastName = aStudent.LastName;
            base.StudentNumber = aStudent.StudentNumber;
            base.ImagePath = aStudent.ImagePath;
            base.IsMale = aStudent.IsMale;
        }
        #endregion

        #region Public Methods

        public Student GetStudent()
        {
            var student = new Student();
            student.ID = this.ID;
            student.ImagePath = this.ImagePath;
            student.IsMale = this.IsMale;
            student.LastName = this.LastName;
            student.Name = this.Name;
            student.StudentNumber = this.StudentNumber;

            return student;

        }
        #endregion
	}
}
