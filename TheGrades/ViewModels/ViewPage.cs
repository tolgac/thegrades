﻿
using Xamarin.Forms;
using Autofac;
using System.Threading.Tasks;

namespace TheGrades
{
	public class ViewPage<T> : ContentPage where T:IViewModel
	{

		#region Ctor
		public ViewPage()
		{
			using (var scope = AppContainer.Container.BeginLifetimeScope())
			{ 
				_viewModel = AppContainer.Container.Resolve<T>();
			}

			BindingContext = _viewModel;
		}
		#endregion

		#region fields
		readonly T _viewModel;
		public T ViewModel { get { return _viewModel; } }
		#endregion
	}
}

