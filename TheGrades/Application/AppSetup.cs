using Autofac;
using System;
using System.Reflection;

namespace TheGrades
{
	public class AppSetup
	{
		public AppSetup()
		{
		}

		public IContainer CreateContainer()
		{
			var containerBuilder = new ContainerBuilder();
			RegisterDependencies( containerBuilder );
			return containerBuilder.Build( Autofac.Builder.ContainerBuildOptions.None );

		}

		protected virtual void RegisterDependencies( ContainerBuilder cb )
		{

			////Register Persistance
			//cb.RegisterGeneric(typeof(Core.Repository<>)).As(typeof(IRepository<>)).WithParameter("SQLiteAsyncConnection",null).InstancePerLifetimeScope();

			#region Register Services
			cb.RegisterType<DBCore.ClassService>().As<TheModel.IClassService>();
			cb.RegisterType<DBCore.SchoolService>().As<TheModel.ISchoolService>();
			cb.RegisterType<DBCore.StudentService>().As<TheModel.IStudentService>();
			cb.RegisterType<DBCore.ClassStudentService>().As<TheModel.IClassStudentService>();
			cb.RegisterType<DBCore.PlusMinusService>().As<TheModel.IPlusMinusService>();
            cb.RegisterType<DBCore.WeeklySchedulewService>().As<TheModel.IWeeklyScheduleService>();
            
			//cb.RegisterType<CourseService>().As<ICourseService>();
			#endregion

			#region Register View Models
//            cb.RegisterType<HomePageViewModel>().SingleInstance();
//			cb.RegisterType<ClassesListPageViewModel>().SingleInstance();
//			cb.RegisterType<AddClassViewModel>().SingleInstance();
			//cb.RegisterType<EditClassViewModel>().SingleInstance();
//			cb.RegisterType<StudentsListViewModel>().SingleInstance();
//			cb.RegisterType<NewStudentViewModel>().SingleInstance();
//            cb.RegisterType<EditStudentViewModel>().SingleInstance();
//            cb.RegisterType<WeeklyScheduleViewModel>().SingleInstance();

			// cb.RegisterType<NewStudentViewModel>().SingleInstance();
			//cb.RegisterType<WeeklySchViewModel>().SingleInstance();
			//cb.RegisterType<SchoolListViewModel>().SingleInstance();
			//cb.RegisterType<NewSchoolViewModel>().SingleInstance();
			//cb.RegisterType<EditSchoolViewModel>().SingleInstance();
			#endregion

		}

	} // AppSetup
} // TheGrader
