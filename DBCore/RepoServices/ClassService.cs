using System.Collections.Generic;
using TheModel;
using System.Threading.Tasks;

namespace DBCore
{
	public class ClassService : TheModel.IClassService
	{
		private readonly IRepository<TheModel.Class> ClassRepo;
		private readonly IRepository<TheModel.Student> StudentRepo;

		public ClassService(IRepository<TheModel.Class> classRepo, IRepository<TheModel.Student> studentRepo)
		{
			
			this.ClassRepo = classRepo;
			this.StudentRepo = studentRepo;
		}

		public Task<Class> GetClassByID(int ID)
		{
			var result = Task.Run(() => ClassRepo.Get(ID));
			return result;
		}

		public   List<Class> GetClassList()
		{
			var result =Task.Run( () => ClassRepo.Get()); 
			return  result.Result;
		}

		public async  Task<int> AddClass(TheModel.Class pClass)
		{

			//Class.Students.Add(1);

			return await ClassRepo.Insert(pClass);
			// var result = Task.Run(() => ClassRepo.Insert(Class));
			//return result.Result;
			 
		}

		public async Task DeleteClass(TheModel.Class pClass)
		{
			await ClassRepo.Delete(pClass);
		}

		public async Task EditClass(TheModel.Class pClass)
		{
			await ClassRepo.Update(pClass);
		}

		//public async Task<int> AddModelClass(Models.NewClassModel Class)
		//{
		//	var domainclass = new TheModel.Class();
		//	domainclass.Name = Class.ClassName;
		//	domainclass.SchoolID = Class.SchoolID;

		//	return await ClassRepo.Insert(domainclass);;
		//}

		public async Task<List<Class>> GetClassesBySchoolID(int SchoolID)
		{
			return await ClassRepo.GetList<Class>(x => x.SchoolID == SchoolID,null);

		}

	}
}

