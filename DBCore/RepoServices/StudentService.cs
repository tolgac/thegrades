using System.Collections.Generic;
using TheModel;
using System.Threading.Tasks;
using System.Linq;
using System;

namespace DBCore
{
	public class StudentService : TheModel.IStudentService
	{	
		#region Ctor
		public StudentService( IRepository<Student> pStudentRepo )
		{
			this._StudentRepo = pStudentRepo;

			//List<Student> GetStudents();
			//Task<int> AddStudent(Student Student);
			//Task DeleteStudent(Student Student);
			//Task EditStudent(Student Student);
		}
		#endregion

		#region private fields
		private readonly IRepository<Student> _StudentRepo;
		#endregion

		#region Methods
		public List<Student> GetStudents()
		{
			var result = Task.Run( () => _StudentRepo.Get() );
			return result.Result;
		}
		public List<Student> GetStudents(System.Linq.Expressions.Expression<Func<Student, bool>> expression)
		{
			var result = Task.Run( () => _StudentRepo.GetList<Student>( expression, null ) );
			return result.Result;
		}

		public async Task<int> AddStudent(Student Student)
		{
			return await this._StudentRepo.Insert(Student);
		}

		public async Task<int> DeleteStudent(Student Student)
		{
			return await this._StudentRepo.Delete(Student);
		}

		public async Task<int> EditStudent(Student Student)
		{
			return await this._StudentRepo.Update(Student);
		}
		public async Task<Student> GetStudentByID(int ID)
		{
			//this.studentRepo.Get();
			return await this._StudentRepo.Get(ID);
		}


		#endregion

	}
}

