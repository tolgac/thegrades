using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TheModel;

namespace DBCore
{
	public class WeeklySchedulewService:IWeeklyScheduleService
	{
 		public WeeklySchedulewService(IRepository<Session> LessonRepo)
		{
			this.SessionRepo = LessonRepo;
		}

		#region private fields
		private readonly IRepository<Session> SessionRepo;
		#endregion

		public List<Session> GetSync()
		{
			var result = Task.Run(() => SessionRepo.Get());
			return result.Result;
		}
		public async Task<int> AddScheduleItem(Session Session)
		{
			return await this.SessionRepo.Insert(Session);

		}

		public async Task<int> DeleteScheduleItem(Session Session)
		{
			return await this.SessionRepo.Delete(Session);

		}
		public async Task<int> DeleteSceduleById(int id)
		{
			var session = await GetScheduleItem(id);
			if (session != null)
			{
				return await this.SessionRepo.Delete(session);
			}
			else
			{
				return -1;
			}
		}
		public async Task<int> EditScheduleItem(Session Session)
		{
			return await this.SessionRepo.Update(Session);

		}

		public async Task<List<Session>> GetSchedules()
		{
			return await SessionRepo.Get();
		}
		public async Task<Session> GetScheduleItem(int id)
		{
			return await this.SessionRepo.Get(id);
		}

	}
}
