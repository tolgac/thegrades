﻿using System;
using System.Collections.Generic;
using SQLite.Net.Async;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace DBCore
{
	public interface IRepository<T> where T: class, new()
	{
		Task<List<T>> Get();
		Task<T> Get(int id);

		Task<List<T>> GetList<TValue>(Expression<Func<T,bool>> predicate =null,Expression<Func<T,TValue>> orderby=null);
		Task<T> Get(Expression<Func<T, bool>> predicate);

		AsyncTableQuery<T> AsQueryable();
		Task<int> Insert(T Entity);
		Task<int> Update(T Entity);
		Task<int> Delete(T Entiyy);
	}
}

