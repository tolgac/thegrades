using SQLite.Net.Attributes;
using System;

namespace TheModel
{
	// TODO tum siniflari kendi cs dosyasina tasimali

	// TODO Aslinda bu Class degil Lesson olmali. 
	public class Class
	{
		[PrimaryKey, AutoIncrement]
		public int ID { get; set; }
		public string ClassName { get; set; }
		public string Grade { get; set; }
		public string Branch { get; set; }
		public int SchoolID { get; set; }
		public string SchoolName { get; set; }
	}

	public class Student
	{
		[PrimaryKey, AutoIncrement]
		public int ID { get; set; }
		public string Name { get; set; }
		public string LastName { get; set; }
		public string StudentNumber { get; set; }
        public string ImagePath { get; set; }
        public bool IsMale { get; set; }

	}

	//persit presentation for coursesesion 	 
	public class School
	{
		[PrimaryKey, AutoIncrement]
		public int ID { get; set; }
		public string Name { get; set; }
		public string City { get; set; }
		public long LessonStartAt { get; set; }
		public int LessonLength { get; set; }

		[Ignore]
		public string LessonStartAtSpan
		{
			get
			{
				return new TimeSpan( LessonStartAt ).ToString( "g" );
			}
		}

		public override string ToString()
		{
			return Name;
		}

		public override bool Equals( object obj )
		{
			School model = obj as School;
			if( model == null || !model.ID.Equals( this.ID ) )
				return false;

			return true;
		}
	}

	public class Behaviors
	{
		public int ID { get; set; }
		public int StudentID { get; set; }
		public int ClassID { get; set; }
		public int BehaviorTypeID { get; set; }
		public long DateTime { get; set; }
	}

	public class BehaviorTypes
	{
		public int ID { get; set; }
		public int BehaviorName { get; set; }
		public bool IsNegative { get; set; }
	}

	public class AbsenceData
	{
		public int StudentID { get; set; }
		public int LessonID { get; set; }
		public DateTime Date { get; set; }
	}

	/*Cross Tables*/
	public class Class_Student
	{
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }

		[Indexed]
		public int ClassID { get; set; }
		public int StudentID { get; set; }

	}

	public class Class_Student_Grades
	{
		[Indexed]
		public int ClassID { get; set; }
		public int StudentID { get; set; }
		public int GradeID { get; set; }
	}
}
