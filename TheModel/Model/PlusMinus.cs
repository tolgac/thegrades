using System;
using SQLite.Net.Attributes;

namespace TheModel
{
    public class PlusMinus
    {
        #region properties aka. table columns
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }

        [Indexed]
        public int ClassID { get; set; }
        [Indexed]
        public int StudentID { get; set; }
        public bool IsPlus { get; set; }
        public DateTime PMDateTime { get; set; }
        #endregion

        #region constructor
        public PlusMinus()
        {
            PMDateTime = DateTime.Now;
        }
        #endregion
    }
}
