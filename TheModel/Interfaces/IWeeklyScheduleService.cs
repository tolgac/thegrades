using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace TheModel
{
    public interface IWeeklyScheduleService
    {
        Task<Session> GetScheduleItem(int id);
        Task<int> AddScheduleItem(Session Lesson);
        Task<int> DeleteScheduleItem(Session Lesson);
        Task<int> DeleteSceduleById(int id);
        Task<List<Session>> GetSchedules();
        Task<int> EditScheduleItem(Session Lesson);
        List<Session> GetSync();
    }
}
