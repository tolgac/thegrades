﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TheModel
{
	public interface IPlusMinusService
	{
		Task<int> NewPlusMinus( PlusMinus aPlusMinus );

		int GetPlusCount( int classID, int studentID );
		int GetMinusCount( int classID, int studentID );

		List<PlusMinus> GetPlusMinusList();
		List<PlusMinus> GetStudentPlusMinusList( int classID, int studentID );
		List<PlusMinus> GetClassPlusMinusList( int classID );
	}
}
