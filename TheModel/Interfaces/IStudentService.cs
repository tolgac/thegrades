using System.Collections.Generic;
using System.Threading.Tasks;
using System;

namespace TheModel
{
	public interface IStudentService
	{
		List<Student> GetStudents();
		Task<int> AddStudent(Student Student);
		Task<int> DeleteStudent(Student Student);
		Task<int> EditStudent(Student Student);
		Task<Student> GetStudentByID(int ID);
		List<Student> GetStudents(System.Linq.Expressions.Expression<Func<Student, bool>> expression);
	}
}

