﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace TheModel
{
	public interface IClassService
	{
		//DTO kullanılabilir

		Task<Class> GetClassByID(int ID);
		List<Class> GetClassList();
		Task<int> AddClass(TheModel.Class Class);
		Task DeleteClass(TheModel.Class Class);
		//Task<int> AddModelClass(TheModel.NewClassModel Class);
		Task<List<Class>> GetClassesBySchoolID(int SchoolID);
		Task EditClass(TheModel.Class Class);
	}
}

